import flask 


# files in app
from app import models


# from __init__.py file
from app import database


'''
LOCAL FUNCTIONS
    - not associated with any routes
'''
class CRUD:
    '''
    CRUD 
        Create
        Read
        Update
        Delete

    '''
    # -----------------------------------------
    # CREATE
    # -----------------------------------------
    def add_entry(new_entry):
        try:
            database.session.add(new_entry)
            database.session.commit()
            return new_entry.id
        except Exception as error:
            database.session.rollback()
            flask.flash(error,"danger")



    # -----------------------------------------
    # READ 
    # -----------------------------------------
    def get_entries():
        try:
            return models.Entry.query.all() # version 2
        except Exception as error:
            database.session.rollback()
            flask.flash(error,"danger")
            return 0


    # ----------------------------------------
    #    READ ONE 
    # ----------------------------------------
    def get_entry(id):
        try:
            # entries = Entry.query.filter(Entry.id==id) version 2
            result = database.get_or_404(models.Entry, id)
            return result
        except Exception as error:
            database.session.rollback()
            flask.flash(error,"danger")
            return 0


    # -----------------------------------------
    # DELETE ALL
    # -----------------------------------------
    def remove_entries():
        try:
            models.Entry.query.delete()
            database.session.commit()
            return 202 # deleted code

        except Exception as error:
            database.session.rollback()
            flask.flash(error,"danger")
            return 404



    # -----------------------------------------
    # DELETE ONE
    # -----------------------------------------
    def remove_entry(id):
        try:
            entry = database.get_or_404(models.Entry, id)
            
            database.session.delete(entry)
            database.session.commit()
            return id

        except Exception as error:
            database.session.rollback()
            flask.flash(error,"danger")
            return 0


