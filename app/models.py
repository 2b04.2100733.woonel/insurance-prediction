#####################################################
############## COMMITS TO FORMS #####################
#####################################################

hello = "world"

from app import database

class Entry(database.Model):
    __tablename__  = "insurances"
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    age = database.Column(database.Integer)
    sex = database.Column(database.String)
    bmi = database.Column(database.Float)
    children = database.Column(database.Integer)
    smoker = database.Column(database.String)
    region = database.Column(database.String)
    prediction = database.Column(database.Float)
    predicted_on = database.Column(database.DateTime, nullable=False)