'''
REST API

This Pytest file is to test the different APIs ONLY 
/api/add
/api/get
/api/get/<id>
/api/delete/<id>
'''

# imports
import pytest
import flask




# the following SHOULD PASS all tests
unexpected_failure_list = [
    #[age, sex     , bmi  , children, smoker, region     , charges ], 
    [ 32 , 'male'  , 28.88, 0       , 'no'  , 'northwest', 3866.86 ], #BMI: Float , Charges: Float
    [ 63 , 'female', 24   , 0       , 'no'  , 'northeast', 14451.83], # BMI : Integer, Charges : Float
    [ 64 , 'male'  , 42   , 5       , 'yes' , 'southwest', 32389   ], # BMI : Integer, Charges : Integer
    [50  , 'male'  , 18.35, 2       , 'no'  , 'northwest', 30284.64294],
    [18  , 'female', 38.17, 0       , 'no'  , 'southeast', 1631.6683],

]


# the following SHOULD FAIL all tests
expected_failure_list = [
    #[age , sex    , bmi  , children, smoker, region     , charges ], 
    [ 32.0, "male" , 28.88, 0       , 'no'  , 'northwest', 3866.86], # float age
    [ 32  , 1      , 28.88, 0       , 'no'  , 'northwest', 3866.86], # numerical sex
    [ 32  , "male" , 28.88, 0.2     , 'no'  , 'northwest', 3866.86], # float children
    [ 32  , "male" , 28.88, 0       , 0     , 'northwest', 3866.86], # numerical smoker
    [ 32  , "male" , 28.88, 0       , 'no'  , 0          , 3866.86], # numerical region
]







'''
VALIDITY TESTING / UNEXPECTED FAILURE TESTING
    Test to make sure all values that are supposed to pass actually pass
'''






# -----------------------------------------------------------------
#    CREATE 
# -----------------------------------------------------------------
global validity_api_test_add_id
# list to store all the added ids
validity_api_test_add_id = []

@pytest.mark.order(1) # create first before anything else
@pytest.mark.parametrize("entrylist", unexpected_failure_list)
@pytest.mark.validity_api_test

def test_api_validity_add(client,entrylist,capsys,):
    with capsys.disabled():

        #prepare the data into a dictionary
        col_names = ["age", "sex", "bmi", "children", "smoker", "region", "prediction"]
        data_body =  dict(zip(col_names, entrylist))


        #data is converted to json
        #posting content is specified
        response = client.post(
            '/api/add', 
            data=flask.json.dumps(data_body), 
            content_type="application/json",
        )

        #check the outcome of the action
        assert response.status_code == 200
        assert response.headers["Content-Type"] == "application/json"

        response_body = flask.json.loads(response.get_data(as_text=True))
        
        # set global variable
        validity_api_test_add_id.append(response_body["id"])

        assert response_body["id"]


# -----------------------------------------------------------------
#    GET ALL
# -----------------------------------------------------------------

@pytest.mark.validity_api_test
def test_api_validity_get_all(client):
    '''
    GET ALL 
        get all results available in table 'predictions'

    .get('/api/get') 
        routes.py api_get_all()
    '''
    response = client.get('/api/get')
    response_body = flask.json.loads(response.get_data(as_text = True))
    assert response_body['results'] is not None


# -----------------------------------------------------------------
#    GET ONE 
# -----------------------------------------------------------------
@pytest.mark.parametrize("index",range(len(unexpected_failure_list)))
@pytest.mark.validity_api_test
def test_api_validity_get_one(client, index, capsys):
    '''
    Create a test client and see if flask is running
    '''
    with capsys.disabled():
        response = client.get(f'/api/get/{validity_api_test_add_id[index]}') 

        assert response.status_code == 200

        # check the outcome of the action
        assert response.headers['Content-Type'] == "application/json"
        response_body = flask.json.loads(response.get_data(as_text = True))
        # assert if response_body is not None
        assert response_body






# -----------------------------------------------------
#     DELETE
# -----------------------------------------------------
@pytest.mark.validity_api_test
@pytest.mark.parametrize("index",range(len(unexpected_failure_list)))
def test_api_validity_delete_one(client, index, capsys):
    
    with capsys.disabled():

        response2 = client.get(f"/api/delete/{validity_api_test_add_id[index]}")

        # check the outcome of the action
        assert response2.status_code == 200

        assert response2.headers['Content-Type'] == "application/json"

        response2_body = flask.json.loads(response2.get_data(as_text = True))
        assert response2_body['result'] == 'ok'









'''
RANGE TESTING 
Test to make sure that values out of range are not allowed

    AGE : min = 18, max = 64
    SEX : ["male", "female"]
    BMI : min = 15.96, max = 53.13
    CHILDREN : min = 0, max = 5
    SMOKER : ["yes", "no"]
    REGION : ["northwest", "northeast", "southwest", "southeast"]
'''


# the following SHOULD FAIL all tests
range_api_testing_expected_failure_list = [
    #[age, sex        , bmi  , children, smoker, region     , charges ], 
    [ 65 , "male"     , 15.96, 0       , 'yes' , 'northwest', 1], # age above minimum
    [ 17 , "female"   , 53.13, 5       , 'no'  , 'northeast', 1], # age below min
    [ 64 , "nonbinary", 15.96, 0       , 'yes' , 'southwest', 1], # invalid sex
    [ 18 , "female"   , 53.14, 5       , 'no'  , 'southeast', 1], # bmi above max
    [ 64 , "male"     , 15.95, 0       , 'yes' , 'northwest', 1], # bmi below max
    [ 18 , "female"   , 53.13, 6       , 'no'  , 'northeast', 1], # children above max
    [ 64 , "male"     , 15.96, -1      , 'yes' , 'southwest', 1], # children below max
    [ 18 , "female"   , 53.13, 5       , 'ok'  , 'southeast', 1], # invalid smoker
    [ 64 , "male"     , 15.96, 0       , 'yes' , 'central'  , 1], # invalid region
]




# -----------------------------------------------------------------
#    CREATE ONE 
# -----------------------------------------------------------------

@pytest.mark.range_api_test
@pytest.mark.parametrize("entrylist", range_api_testing_expected_failure_list)
@pytest.mark.xfail(reason="RANGE TESTING - arguments invalid/out of range")

def test_api_range_add(client, entrylist, capsys): test_api_validity_add(client, entrylist, capsys)




# -----------------------------------------------------------------
#    GET ALL
#        unexpected failure
# -----------------------------------------------------------------
# @pytest.mark.parametrize("entrylist", unexpected_failure_list)

@pytest.mark.range_api_test
def test_api_range_get_all(client):
    '''
    GET ALL 
        get all results available in table 'predictions'

    AGE : min = 18, max = 64
    SEX : ["male", "female"]
    BMI : min = 15.96, max = 53.13
    CHILDREN : min = 0, max = 5
    SMOKER : ["yes", "no"]
    REGION : ["northwest", "northeast", "southwest", "southeast"]
    '''
    response = client.get('/api/get') 
    assert response.status_code == 200
    response_body = flask.json.loads(response.get_data(as_text=True))['results']
    
    for record in response_body: 
        assert record['age'] >= 18 and  record['age'] <= 64
        assert record['sex'] in ["male", "female"]
        assert record['bmi'] >= 15.96 and record['bmi'] <= 53.13
        assert record['children'] >= 0 and  record['children'] <= 5
        assert record['smoker'] in ["yes", "no"]
        assert record['region'] in ["northwest", "northeast", "southwest", "southeast"]
        
