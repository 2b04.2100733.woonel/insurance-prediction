# Build from latest python image
FROM python:3.10

# Update packeges installed in the image
RUN apt-get update -y

# to fix gcc error during build
# RUN apt intall gcc -y
RUN apt-get install build-essential -y

#Change our working directory to app folder
WORKDIR /app-docker

COPY . .

# upgrade pip first
RUN pip3 install pip==22.3.1

#Install requirements.txt in virtual environemnt
RUN pip3 install -r requirements.txt

# Add every files and folder into the app folder
# Expose port 5000 for http communication
EXPOSE 5000
# Run gunicorn web server and binds it to the port
CMD python3 -m flask run --host=0.0.0.0