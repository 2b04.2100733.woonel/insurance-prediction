
#flask import
import flask

import datetime



'''
LOCAL IMPORTS
    from local files
'''
from app import  forms, models, CRUD, Preprocessor


'''
GLOBAL VARIABLES
    SEE __init__.py for more details
'''
from app import app, ai_model, database



'''
INDEX / PREDICTION
    /index
    /home
    /

'''
@app.route('/index', methods = ['GET'])
@app.route('/home', methods = ['GET'])
@app.route('/', methods = ['GET'])

def index_get():

    # if user has NEVER logged on before, proceed with login
    if flask.request.cookies.get('email') is None: 
        return flask.redirect("/login")

    # if user has logged in
    else:
        prediction_form = forms.PredictionForm()
        return flask.render_template(
            "index.html", 
            form=prediction_form, 
            title="Index",
            index=True, 
            login = False,
            user = False,
            )




@app.route('/index', methods = ['POST'])
@app.route('/home', methods = ['POST'])
@app.route('/', methods = ['POST'])

def index_post():

    # if user has NEVER logged on before, proceed with login
    if flask.request.cookies.get('email') is None: 
        return flask.redirect("/login")

    
    else: # if user has logged in
        prediction_form = forms.PredictionForm()
       

        if prediction_form.validate_on_submit(): # IF user passes validation
            col_names = ['age', 'sex', 'bmi', 'children', 'smoker', 'region' ]


            values = [
                prediction_form.data['age'],
                prediction_form.data['sex'],
                prediction_form.data['bmi'],
                prediction_form.data['children'],
                'yes' if prediction_form.data['smoker'] else 'no',
                prediction_form.data['region'][0],
            ]




            prediction_data = Preprocessor.Preprocessor.db_to_pd(values, order_cols=col_names)


            X = Preprocessor.Preprocessor.preprocessor(prediction_data)


            result = format(ai_model.predict(X)[0], '.2f')

            flask.flash(f"Predicted Insurance Charge: ${result}","success")
            

            prediction_data['prediction'] = result
            prediction_data['predicted_on'] = datetime.datetime.utcnow()
            
            new_entry = models.Entry(**prediction_data.to_dict('records')[0])


            CRUD.CRUD.add_entry(new_entry)

            
        else: # IF user FAILS validation
            
            flask.flash("Error, cannot proceed with prediction","danger")
        return flask.render_template(
            "index.html", 
            form=prediction_form, 
            title="Index",
            index=True, 
            login = False,
            user = False,
            )






'''
LOGIN
'''
@app.route('/login', methods = ['GET'])
def login_get():

    # if user has NEVER logged on before, proceed with login
    if flask.request.cookies.get('email') is None: 
        
        login_form = forms.LoginForm()
        return flask.render_template(
            "login.html", 
            form=login_form, 
            title="Login",
            index=False, 
            login = True, 
        )

    # if user has logged on before, go straight to index.html
    else: 
        return flask.redirect("/")





@app.route('/login', methods = ['POST'])
def login_post():
    login_form = forms.LoginForm()
    if flask.request.method == 'POST':

        # validate_on_submit basically validates if its string and if both are not empty field


        if login_form.validate_on_submit():
            '''
            validate_on_submit()
                EMAIL
                    validators.InputRequired
                    validators.Email
                    validators.AnyOf( values=["kohqichang@mail.com"] )

                PASSWORD
                    validators.InputRequired
                    validators.AnyOf( values=["password1"] )

                SEE forms.py class LoginForm() for more details
            '''
            email = login_form.email.data
            password = login_form.password.data



            # -----------------------------------------------------
            #    SET COOKIE FOR EMAIL
            # -----------------------------------------------------
            resp = flask.make_response(flask.redirect("/"))
            resp.set_cookie('email', str(email))
            return resp

        # failed validate_on_submit()
        else:
            flask.flash("Please Enter Valid Email/Password", "danger")
            return flask.render_template(
                    "login.html", 
                    title="Login", 
                    form=login_form, 
                    index=False, 
                    login = True, 

                )






'''
VIEW USER HISTORY
'''
@app.route('/userhistory', methods = ['GET'])

def userhistory_get():

    # if user has NEVER logged on before, proceed with login
    if flask.request.cookies.get('email') is None: 
        return flask.redirect("/login")

    # if user has logged in
    else:
        return flask.render_template(
            "user.html", 
            title="User History",
            entries = CRUD.CRUD.get_entries(),
            index=False, 
            login = False,
            user = True,
            )




## Deleting user history

@app.route('/userhistory', methods = ['POST'])

def userhistory_post():

    # if user has NEVER logged on before, proceed with login
    if flask.request.cookies.get('email') is None: 
        return flask.redirect("/login")

    # if user has logged in
    else:
        id = flask.request.form['id']

        # remove entry
        CRUD.CRUD.remove_entry(id)


        return flask.render_template(
            "user.html", 
            entries = CRUD.CRUD.get_entries(), 
            title="User History",
            index=False, 
            login=False, 
            user = True,
            )




## Deleting ALL USER HISTORY 

@app.route('/removeEntries', methods = ['POST'])

def removeEntries_post():

    # if user has NEVER logged on before, proceed with login
    if flask.request.cookies.get('email') is None: 
        return flask.redirect("/login")

    # if user has logged in
    else:

        # remove entry
        response_code  = CRUD.CRUD.remove_entries()
        if response_code == 202:
            pass 

        else: # IF fail to delete everything
            flask.flash("Error, cannot proceed with Deletion","danger")
        return flask.redirect(flask.url_for("userhistory_get"))
        




# ------------------------------------
## LOGOUT
# ------------------------------------

@app.route('/logout', methods = ['POST'])

def logout_post():

    # if user has NEVER logged on before, proceed with login
    if flask.request.cookies.get('email') is None: 
        return flask.redirect("/login")

    # if user has logged in
    else:

        # Delete cookie & Go to Log in page
        resp = flask.make_response(flask.redirect("/login"))
        resp.set_cookie('email', '', max_age=0)
        return resp




















'''
UNIT TESTING API 
'''
# ------------------------------------
#    ADD
# ------------------------------------

@app.route("/api/add", methods = ["POST"])
def api_add():
    #retrieve the json file posted from client
    prediction_data = flask.request.get_json()
    #retrieve each field from the data


    ## Range Validation
    if (
        # AGE : min = 18, max = 64
        ( prediction_data["age"] >= 18 and prediction_data["age"] <=64) and
        # SEX : ["male", "female"]
        ( prediction_data["sex"] in ["male", "female"] ) and 
        # BMI : min = 15.96, max = 53.13
        ( prediction_data["bmi"] >= 15.96 and prediction_data["bmi"] <= 53.13 ) and
        # CHILDREN : min = 0, max = 5
        ( prediction_data["children"] >= 0 and prediction_data["children"] <=5 ) and
        # SMOKER : ["yes", "no"]
        ( prediction_data["smoker"] in ["yes", "no"] ) and
        # REGION : ["northwest", "northeast", "southwest", "southeast"]
        ( prediction_data["region"] in ["northwest", "northeast", "southwest", "southeast"])
    ):
        pass # pass validation
    else : 
        flask.abort(404) # if fail validation, throw a error (invalid user input)
    

    #create an Entry object store all data for db action


    prediction_data["predicted_on"] = datetime.datetime.utcnow()
    new_entry = models.Entry(**prediction_data)

    result = CRUD.CRUD.add_entry(new_entry)
        
    #return the result of the db ac
    return flask.jsonify({"id" : result})







# ------------------------------------
#    READ MANY
# ------------------------------------
@app.route("/api/get", methods=['GET'])
def api_get_all():
    #retrieve the entry using id from client
    try: 
        entries = CRUD.CRUD.get_entries()



        #Prepare a dictionary for json conversion
        col_names = ['id', 'age','sex','bmi','children','smoker','region','prediction',]
        
        
        results = []
        # put data into dictionary body
        for entry in entries:

            # data entry
            data = dict(zip(col_names, [getattr(entry, col_name) for col_name in col_names]))

            results.append(data)
        
        #Convert the data to json
        return flask.jsonify({'results' : results}) #response back
    except: 
        return flask.jsonify({'results' : None})







# ------------------------------------
#    READ ONE
# ------------------------------------
@app.route("/api/get/<id>", methods=['GET'])
def api_get_one(id):
    #retrieve the entry using id from client
    entry = CRUD.CRUD.get_entry(int(id))
    #Prepare a dictionary for json conversion
    col_names = ['id', 'age','sex','bmi','children','smoker','region','prediction',]
    
    # put data into dictionary body
    data = dict(zip(col_names, [getattr(entry, col_name) for col_name in col_names]))

    #Convert the data to json
    result = flask.jsonify(data)
    return result #response back







# ------------------------------------
#    DELETE
# ------------------------------------
@app.route("/api/delete/<id>", methods=['GET'])
def api_delete(id):
    entry = CRUD.CRUD.remove_entry(int(id))
    return flask.jsonify({'result':'ok'})











